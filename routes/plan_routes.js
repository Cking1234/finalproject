var express = require('express');
var router = express.Router();
var plan_dal = require('../model/plan_dal');



// View All plans
router.get('/all', function(req, res) {
    plan_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('plan/planViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){

        plan_dal.getById(req.query.plan_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('plan/planViewById', {'result': result});
            }
        });

});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    address_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('plan/planAdd', {'address': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.plan_name == null) {
        res.send('Plan Name must be provided.');
    }
    else if(req.query.address_id == null) {
        res.send('At least one address must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        plan_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/plan/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.plan_id == null) {
        res.send('A plan id is required');
    }
    else {
        plan_dal.edit(req.query.plan_id, function(err, result){
            res.render('plan/planUpdate', {plan: result[0][0], address: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.company_id == null) {
        res.send('A company id is required');
    }
    else {
        company_dal.getById(req.query.company_id, function(err, company){
            address_dal.getAll(function(err, address) {
                res.render('company/companyUpdate', {company: company[0], address: address});
            });
        });
    }

});

router.get('/update', function(req, res) {
    plan_dal.update(req.query, function(err, result){
        res.redirect(302, '/plan/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.plan_id == null) {
        res.send('plan_id is null');
    }
    else {
        plan_dal.delete(req.query.plan_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/plan/all');
            }
        });
    }
});

module.exports = router;
