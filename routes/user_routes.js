var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');
var address_dal = require('../model/address_dal');
var plan_dal = require('../model/plan_dal');



router.get('/aboutUs', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    user_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('user/userAbout', {user: result});
        }
    });
});





router.get('/userInfo', function(req, res){
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    else {
        user_dal.getById(req.query.user_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('user/userViewById', {'result': result[0]});
                //res.render('user/userViewById', {user: result[0][0], plan: result[1], address: result[2]});

            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    plan_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('user/userAdd', {'plan': result});
        }
    });
});


router.get('/selectUser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    user_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('user/userLogin', {user: result});
        }
    });
});


// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.username == null) {
        res.send('Username must be provided.');
    }
    else if(req.query.street_no == null) {
        res.send('No Street Number entered');
    }
    else if(req.query.street_no == null) {
        res.send('No Street Name entered');
    }
    else if(req.query.street_no == null) {
        res.send('No City entered');
    }
    else if(req.query.street_no == null) {
        res.send('No State entered');
    }
    else if(req.query.street_no == null) {
        res.send('No Zip Code entered');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        user_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.user_id == null) {
        res.send('A user id is required');
    }
    else {
        user_dal.edit(req.query.user_id, function(err, result){
            res.render('user/userUpdate', {user: result[0][0], plan: result[1]});
        });
    }

});



router.get('/update', function(req, res) {
    user_dal.update(req.query, function(err, result){
       res.redirect(302, '/user/selectuser');
    });
});



module.exports = router;
