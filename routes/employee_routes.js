var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');
var address_dal = require('../model/address_dal');
var plan_dal = require('../model/plan_dal');
var employee_dal = require('../model/employee_dal');


// View All companys
router.get('/all', function(req, res) {
    employee_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeViewAll', { 'result':result });
        }
    });

});





router.get('/employeeInfo', function(req, res){
    if(req.query.user_id == null) {
        res.send('employee_id is null');
    }
    else {
        employee_dal.getById(req.query.user_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('employee/employeeViewById', {'result': result[0]});
                //res.render('user/userViewById', {user: result[0][0], plan: result[1], address: result[2]});

            }
        });
    }
});


router.get('/viewMaint', function(req, res) {
    employee_dal.getAllMaint(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeMaintViewAll', { 'result':result });
        }
    });

});


router.get('/yearMaintenance', function(req, res) {
    employee_dal.yearlyMaint(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeMaintThisYear', { 'result':result });
        }
    });

});






router.get('/addMaint', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    employee_dal.addMaint(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeMaintenanceAdd', {employee: result[0], user: result[1], maintenance: result[2], plan: result[3]});
        }
    });
});



router.get('/selectEmployee', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    employee_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeLogin', {employee: result});
        }
    });
});


// View the company for the given id
router.get('/insertMaint', function(req, res){

        // passing all the query parameters (req.query) to the insert function instead of each individually
        employee_dal.insertMaint(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/employee/viewMaint');
            }
        });

});








module.exports = router;
