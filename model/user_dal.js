var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);





exports.getAll = function(callback) {
    var query = 'SELECT * FROM user1 ' +
        'ORDER BY username';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(user_id, callback) {
    var query = 'CALL view_user11(?)'
    var queryData = [user_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE USER
    var query = 'INSERT INTO user1 (username) VALUES (?)';
    var queryData = [params.username];




    connection.query(query, queryData, function(err, result) {

        // THEN USE THE USER_ID RETURNED AS insertId AND THE SELECTED A
        var user_id = result.insertId;

        // Create new address with info
        var query1 = 'INSERT INTO address1 (street_no, street_name, city, state, zip) VALUES ?';
        //var queryData1 = [params.street_no]
        var newUserData = [];
        newUserData.push([ params.street_no, params.street_name, params.city, params.state, params.zip]);


        //UPDATE NEW ADDRESS_ID FOR NEW USEr
        connection.query(query1, [newUserData], function(err, result){
            //get newly created address_id
            var address_id = result.insertId;
            var query2 = 'UPDATE user1 SET address_id = ? WHERE user_id = ?';
            var queryData2 = [address_id, user_id];

            //var newUserData2 = [];
            //newUserData2.push([ address_id, user_id]);

            connection.query(query2, queryData2, function(err, result){
                var query3 = 'UPDATE user1 SET plan_id = ?, password = ?, first_name = ?, last_name = ?, creditcard = ? WHERE user_id = ?';
                var queryData3 = [params.plan_id, params.password, params.first_name, params.last_name, params.creditcard, user_id];

                connection.query(query3, queryData3, function(err, result){
                    callback(err, result);
                });
            });
        });
    });

};




exports.update = function(params, callback) {
    var query = 'UPDATE user1 SET first_name = ?, last_name = ?, password = ?, creditcard = ?, plan_id = ? WHERE user_id = ?';
    var queryData = [params.first_name, params.last_name, params.password, params.creditcard, params.plan_id, params.user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.edit = function(user_id, callback) {
    var query = 'CALL edit_user1(?)'
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};