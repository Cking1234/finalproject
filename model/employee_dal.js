var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM employee1 ' +
                'ORDER BY last_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};



exports.getAllMaint = function(callback) {
    var query = 'SELECT * FROM allMaint_view ' ;

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.yearlyMaint = function(callback) {
    var query = 'SELECT num_maintenances_year(user_id) AS num_maintenances_this_year, user_id, username FROM user1; ' ;



    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.addMaint = function(callback) {
    var query = 'CALL add_maint1() ';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(user_id, callback) {
    var query = 'CALL view_employee1(?)';
    var queryData = [employee_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insertMaint = function(params, callback) {

    var query = 'INSERT INTO maintenance1 (employee_id, user_id, description) VALUES ?';
    //var queryData1 = [params.street_no]
    var newMaintData = [];
    newMaintData.push([ params.employee_id, params.user_id, params.description]);

    connection.query(query, [newMaintData], function(err, result) {

        var query1 = 'UPDATE user1 SET plan_id = ? WHERE user_id = ?';
        var queryData1 = [params.user_id, params.plan_id];
        connection.query(query1, queryData1, function(err, result) {
            callback(err, result);
        });



    });

};


